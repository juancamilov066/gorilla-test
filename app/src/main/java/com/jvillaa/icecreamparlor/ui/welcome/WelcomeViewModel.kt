package com.jvillaa.icecreamparlor.ui.welcome

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jvillaa.icecreamparlor.data.datasource.ProductsRemoteDataSource
import com.jvillaa.icecreamparlor.data.model.ApiProductModel
import com.jvillaa.icecreamparlor.utils.Result
import kotlinx.coroutines.launch
import javax.inject.Inject

class WelcomeViewModel @Inject constructor(
    private val productsRemoteDataSource: ProductsRemoteDataSource
) : ViewModel() {

    private val productsMediatorLiveData: MediatorLiveData<Result<List<ApiProductModel>>> = MediatorLiveData()
    val productsLiveData: LiveData<Result<List<ApiProductModel>>> get() = productsMediatorLiveData

    fun getProducts() {
        viewModelScope.launch {
            productsMediatorLiveData.postValue(
                productsRemoteDataSource.getProducts()
            )
        }
    }

}