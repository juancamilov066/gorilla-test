package com.jvillaa.icecreamparlor.ui.receipt

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.jvillaa.icecreamparlor.R
import com.jvillaa.icecreamparlor.data.model.ApiProductModel
import com.jvillaa.icecreamparlor.ui.welcome.WelcomeActivity
import kotlinx.android.synthetic.main.activity_receipt.*


class ReceiptActivity : AppCompatActivity() {

    private lateinit var order: MutableList<ApiProductModel>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_receipt)

        val order = intent.getSerializableExtra("order") as ArrayList<ApiProductModel>
        this.order = order.toMutableList()

        listOrder()

        newOrderButton.setOnClickListener {
            startNewOrder()
        }
    }

    private fun listOrder() {
        var orderText = ""
        var total = 0.0
        order.forEach {
            orderText += "Name: ${it.nameOne}, Count: ${it.count}, Price ${it.price} \n"
            val price = it.price.replace("$", "").toDouble()
            total += price
        }
        orderText += "TOTAL $total"

        orderTextView.text = orderText
    }

    private fun startNewOrder() {
        val intent = Intent(this, WelcomeActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }
}
