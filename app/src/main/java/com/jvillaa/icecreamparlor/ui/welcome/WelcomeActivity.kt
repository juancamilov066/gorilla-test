package com.jvillaa.icecreamparlor.ui.welcome

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.jvillaa.icecreamparlor.R
import com.jvillaa.icecreamparlor.data.model.ApiProductModel
import com.jvillaa.icecreamparlor.ui.adapter.ProductsAdapter
import com.jvillaa.icecreamparlor.ui.receipt.ReceiptActivity
import com.jvillaa.icecreamparlor.utils.OnListClickListener
import com.jvillaa.icecreamparlor.utils.Result
import com.jvillaa.icecreamparlor.utils.component
import kotlinx.android.synthetic.main.activity_welcome.*
import javax.inject.Inject


class WelcomeActivity : AppCompatActivity(), OnListClickListener<ApiProductModel> {

    @Inject
    lateinit var welcomeViewModel: WelcomeViewModel

    private val productsToOrder: MutableList<ApiProductModel> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
        setSupportActionBar(welcomeToolbar)

        component.inject(this)

        orderButton.isEnabled = false
        orderButton.setOnClickListener {
            val intent = Intent(this, ReceiptActivity::class.java)
            intent.putParcelableArrayListExtra("order", ArrayList(productsToOrder))
            startActivity(intent)
        }

        observe()
        welcomeViewModel.getProducts()
    }

    private fun observe() {
        welcomeViewModel.productsLiveData.observe(this, Observer {
            when (it) {
                is Result.Success -> {
                    setProductsList(it.data)
                }
                is Result.Error -> {
                    Toast.makeText(this, it.exception.message, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun setProductsList(products: List<ApiProductModel>) {
        productsRecyclerView.layoutManager = GridLayoutManager(this, 2)
        val adapter = ProductsAdapter(this, products)
        adapter.setOnListClickListener(this)
        productsRecyclerView.adapter = adapter
    }

    override fun onClick(item: ApiProductModel) {
        if (productsToOrder.size == 0) {
            orderButton.isEnabled = true
            item.count = 1
            productsToOrder.add(item)
            return
        }

        val clonedProductsToOrder = productsToOrder.toMutableList()

        clonedProductsToOrder.forEach {
            if (it.nameOne == item.nameOne) {
                if (item.count == 1) {
                    productsToOrder.remove(item)
                    item.count = 2
                    productsToOrder.add(item)
                } else {
                    productsToOrder.remove(item)
                    if (productsToOrder.size == 0) orderButton.isEnabled = false
                }
            } else {
                item.count = 1
                productsToOrder.add(item)
            }
        }
    }
}
