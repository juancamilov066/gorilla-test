package com.jvillaa.icecreamparlor.ui.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.jvillaa.icecreamparlor.R
import com.jvillaa.icecreamparlor.data.model.ApiProductModel
import com.jvillaa.icecreamparlor.utils.ImageSelector
import com.jvillaa.icecreamparlor.utils.OnListClickListener
import kotlinx.android.synthetic.main.item_product.view.*

class ProductsAdapter(val context: Context, private val products: List<ApiProductModel>) :
    RecyclerView.Adapter<ProductsAdapter.ProductsViewHolder>() {

    private lateinit var listClickListener: OnListClickListener<ApiProductModel>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductsViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_product, parent, false)
        return ProductsViewHolder(view)
    }

    override fun getItemCount(): Int =
        products.size

    override fun onBindViewHolder(holder: ProductsViewHolder, position: Int) {
        val product = products[position]
        holder.bind(product)
    }

    fun setOnListClickListener(onListClickListener: OnListClickListener<ApiProductModel>) {
        this.listClickListener = onListClickListener
    }

    inner class ProductsViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(product: ApiProductModel) {
            view.setOnClickListener {
                listClickListener.onClick(product)
            }
            view.productImageView.setImageDrawable(
                ContextCompat
                    .getDrawable(context, ImageSelector.getImageResourceBasedOnType(product.type))
            )
            view.priceTextView.text = product.price
            view.productNameTextView.text = product.nameOne
            view.productImageView.setBackgroundColor(Color.parseColor(product.bgColor))
        }
    }
}