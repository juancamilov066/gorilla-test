package com.jvillaa.icecreamparlor.di.modules

import android.app.Application
import com.jvillaa.icecreamparlor.BaseApplication
import com.jvillaa.icecreamparlor.data.datasource.ProductsRemoteDataSource
import com.jvillaa.icecreamparlor.data.datasource.ProductsRemoteDataSourceImpl
import com.jvillaa.icecreamparlor.data.network.ProductApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideApplication(application: BaseApplication): Application {
        return application
    }

    @Provides
    @Singleton
    fun provideProductsApi(retrofit: Retrofit): ProductApi {
        return retrofit.create(ProductApi::class.java)
    }

    @Provides
    @Singleton
    fun provideProductsRemoteDataSource(
        productApi: ProductApi
    ): ProductsRemoteDataSource {
        return ProductsRemoteDataSourceImpl(productApi)
    }

}