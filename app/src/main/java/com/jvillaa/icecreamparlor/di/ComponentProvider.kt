package com.jvillaa.icecreamparlor.di

import com.jvillaa.icecreamparlor.di.components.AppComponent

interface ComponentProvider {
    val component: AppComponent
}