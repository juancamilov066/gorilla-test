package com.jvillaa.icecreamparlor.di.components

import com.jvillaa.icecreamparlor.di.modules.AppModule
import com.jvillaa.icecreamparlor.di.modules.NetworkModule
import com.jvillaa.icecreamparlor.di.modules.ViewModelModule
import com.jvillaa.icecreamparlor.ui.welcome.WelcomeActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class,
    NetworkModule::class,
    ViewModelModule::class
])
interface AppComponent {
    fun inject(welcomeActivity: WelcomeActivity)
}