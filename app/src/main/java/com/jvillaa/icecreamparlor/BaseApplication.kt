package com.jvillaa.icecreamparlor

import android.app.Application
import com.jvillaa.icecreamparlor.di.ComponentProvider
import com.jvillaa.icecreamparlor.di.components.AppComponent
import com.jvillaa.icecreamparlor.di.components.DaggerAppComponent

class BaseApplication : Application(), ComponentProvider {
    override val component: AppComponent by lazy {
        DaggerAppComponent.create()
    }
}