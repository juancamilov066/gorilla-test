package com.jvillaa.icecreamparlor.data.datasource

import com.jvillaa.icecreamparlor.data.model.ApiProductModel
import com.jvillaa.icecreamparlor.data.network.ProductApi
import com.jvillaa.icecreamparlor.utils.Result
import java.lang.Exception

class ProductsRemoteDataSourceImpl(
    private val productApi: ProductApi
) : ProductsRemoteDataSource {

    override suspend fun getProducts(): Result<List<ApiProductModel>> {
        try {
            val response = productApi.getProducts()
            if (response.isSuccessful) {
                val body = response.body()
                return if (body != null) {
                    Result.Success(body)
                } else {
                    Result.Error(
                        Exception("There was an error fetching the products: ${response.code()}")
                    )
                }
            }
            return Result.Error(
                Exception("There was an error fetching the products: ${response.code()}")
            )
        } catch (e: Exception) {
            return Result.Error(
                Exception("There was an error fetching the products: $e")
            )
        }
    }
}