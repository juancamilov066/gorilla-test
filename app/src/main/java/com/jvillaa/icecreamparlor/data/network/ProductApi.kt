package com.jvillaa.icecreamparlor.data.network

import com.jvillaa.icecreamparlor.data.model.ApiProductModel
import retrofit2.Response
import retrofit2.http.GET

interface ProductApi {

    @GET("products")
    suspend fun getProducts(): Response<List<ApiProductModel>>
}