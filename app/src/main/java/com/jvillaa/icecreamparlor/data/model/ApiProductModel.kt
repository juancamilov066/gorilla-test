package com.jvillaa.icecreamparlor.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ApiProductModel (
    @Expose
    @SerializedName("name1")
    val nameOne: String,
    @Expose
    @SerializedName("name2")
    val nameTwo: String,
    @Expose
    val price: String,
    @Expose
    @SerializedName("bg_color")
    val bgColor: String,
    @Expose
    val type: String,
    var count: Int = 0
) : Parcelable {
    
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readInt()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(nameOne)
        parcel.writeString(nameTwo)
        parcel.writeString(price)
        parcel.writeString(bgColor)
        parcel.writeString(type)
        parcel.writeInt(count)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ApiProductModel> {
        override fun createFromParcel(parcel: Parcel): ApiProductModel {
            return ApiProductModel(parcel)
        }

        override fun newArray(size: Int): Array<ApiProductModel?> {
            return arrayOfNulls(size)
        }
    }

}