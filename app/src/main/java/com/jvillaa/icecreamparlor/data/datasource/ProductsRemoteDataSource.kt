package com.jvillaa.icecreamparlor.data.datasource

import com.jvillaa.icecreamparlor.data.model.ApiProductModel
import com.jvillaa.icecreamparlor.utils.Result

interface ProductsRemoteDataSource {
    suspend fun getProducts(): Result<List<ApiProductModel>>
}