package com.jvillaa.icecreamparlor.utils

import android.app.Activity
import com.jvillaa.icecreamparlor.BaseApplication
import com.jvillaa.icecreamparlor.di.components.AppComponent

val Activity.component: AppComponent get() = (application as BaseApplication).component