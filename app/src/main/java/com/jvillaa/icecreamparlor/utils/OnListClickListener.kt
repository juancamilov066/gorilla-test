package com.jvillaa.icecreamparlor.utils

interface OnListClickListener<T> {
    fun onClick(item: T)
}