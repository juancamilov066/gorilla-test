package com.jvillaa.icecreamparlor.utils

import com.jvillaa.icecreamparlor.R

object ImageSelector {
    fun getImageResourceBasedOnType(type: String): Int {
        return when (type) {
            "popsicle" -> R.drawable.popsicle
            "cone" -> R.drawable.cone
            "froyo" -> R.drawable.froyo
            "sundae" -> R.drawable.ice_cream
            else -> R.drawable.cone
        }
    }
}